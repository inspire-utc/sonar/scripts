# This program transforms the scan lines to points in 3D space
# 1) Use Seanet Dumplog to dump .V4Log to CSV. Select only sonar data in the GUI
# 2) Pipe the csv file to this program
# 3) The output of this file is a CSV. with XYZ columns

import csv
import sys
import math

DBYTES = 14
BEARING = 13
RIGHTLIM = 11

def main():
    reader = csv.reader(sys.stdin)
    next(reader) # Strip the header     
    row = next(reader) 
    nbytes = int(row[DBYTES])
    right_lim = int(row[RIGHTLIM])
    bearing_mp = 2*math.pi / right_lim # slice unit circle 

    writer = csv.writer(sys.stdout)    
    
    for i, row in enumerate(reader):
        if int(row[DBYTES]) != nbytes:
            continue

        data = reversed([int(x) for x in row[DBYTES+1:nbytes]])
        theta = int(row[BEARING]) * bearing_mp
        for radius, z in enumerate(data):
            # Apply transformation, the write to file
            writer.writerow([radius*math.cos(theta), radius*math.sin(theta), z])

if __name__ == "__main__":
    main()
