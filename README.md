# Sonar Scripts

This repository contains various scripts to process sonar data

## contour.py

This script is used to transform scan line data from a CSV file into coordinates in 3D space

```
python3 contour.py < data/pt_21_paz_11_33.csv > points.csv
```

